dropNthElement(List,N):-
    %length(List,KeepCount),
    drop(List,N,0,NewList).

drop([],_,_,NewList):-
    write(NewList).
drop([H|T],N,KeepCount,NewList):-
    NewKeepCount is KeepCount + 1,
    Remainder is NewKeepCount mod N,
    % write(" "),
    % write(Remainder),
    (Remainder == 0 ->drop(T,N,NewKeepCount,NewList); append(NewList, [H], TempList),drop(T,N,NewKeepCount,TempList)).
