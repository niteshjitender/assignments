%Q1 and Q2 and Q4

remove_way1():-
    retractall(db1(_)).
%It will always return true... no matter the fact has some value or not...

%retractall(female(sita)) removes only sita
%retractall(female(_)) and retractall(female(X)) are same ,remove all the fact 'female'

remove_way2(X):-
    retract(db1(X)).
%It will return true if the fact with value is succesfully removed, Otherwise false

%retract(X) print the value of X and give choice to user for further deletion,
%retract(_) same as above but not print any value

remove_way3():-
    retract(db1(_)).

