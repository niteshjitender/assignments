:- use_module(library(chr)).
:- chr_constraint red,blue,green,yellow,a,b, term/1.

red\true <=> write(1).


% red,red,yellow ==> write(2).

% term(X),term(Y),term(Z) ==> write(1).
a ==> write(b),retract(db(X)),X \=b |assert(db(a)) ,b.
b ==> write(a), retract(db(X)),X \= a |assert(db(b)) ,a.
