find_last_Element([H|[]]):-
    Ans = H,
    write(Ans).
find_last_Element([H|List]):-
    find_last_Element(List).

%---------------------------------------------------

find_last_Element_2(List):-
    length(List,Len),
    nth1(Len,List,Element),
    write(Element).