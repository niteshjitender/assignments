output(List):-
    length(List,Len),
    Power is Len - 1,
    Len =\= 0 -> display(List,Power) ; write('0').

display([],_).
display([H|List],Power):- 
    NewPower is Power - 1,
    (H =:= 0 -> display(List,NewPower);
                (H < 0 -> write(H); write(+), write(H)),
                          write("x^"),write(Power),write(' '),
                          display(List,NewPower)
    ). 
    
