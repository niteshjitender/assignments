isElementPresent(X,[],"Not Present").
isElementPresent(X,[H|List],A):-
    H =:= X -> A = "Present"; isElementPresent(X,List,A).

%-----------------------------------------------------------------------

isElementPresent_2(X,[]):-
    write("Element not Found").

isElementPresent_2(H,[H|T]):-
    write("Element Found").
isElementPresent_2(Y,[H|T]):-
    isElementPresent_2(Y,T).

%-------------------------------------------------------------------------

isElementPresent_3(X,List):-
    member(X, List).
    
%------------------------------------------------------------------------