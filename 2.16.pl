%___________________________________________________________________________________________________________________________

transpose_Matrix([H|Matrix],FinalAnswer):-
    length(H,LenOfRow),
    master([H|Matrix],0,LenOfRow,[],FinalAnswer).

master(Matrix,Index,0,NewTempAnswer,FinalAnswer):-
    FinalAnswer = NewTempAnswer.
master(Matrix,Index,LenOfRow,TempAnswer,FinalAnswer):-
    worker(Matrix,Index,[],FinalList),
    NewIndex is Index + 1,
    New_LenOfRow is LenOfRow - 1,
    append(TempAnswer,[FinalList],NewTempAnswer),
    master(Matrix,NewIndex,New_LenOfRow,NewTempAnswer,FinalAnswer),!.

worker([],Index,NewTempList,FinalList):-
    FinalList = NewTempList.
worker([H|Matrix],Index,TempList,FinalList):-
    nth0(Index,H,Element),
    append(TempList,[Element],NewTempList),
    worker(Matrix,Index,NewTempList,FinalList),!.
    
multiplyMatrix(Matrix1,Matrix2,Answer):-
    transpose_Matrix(Matrix2,TransposedMatrix),
    multiplyWorker(Matrix1,TransposedMatrix,Answer).

multiplyWorker([],_,[]).
multiplyWorker([Row|Rows],TransposedMatrix,[Result|Answer]):- multiply_ListWithMatrix(Row,TransposedMatrix,Result),
                                                              multiplyWorker(Rows,TransposedMatrix,Answer).
                                                    
multiply_ListWithMatrix(_,[],[]).
multiply_ListWithMatrix(L1,[L2|T],[Result|Row]):- multiply_TwoLists(L1,L2,Result),
                                                  multiply_ListWithMatrix(L1,T,Row),!.
multiply_TwoLists([],_,0).
multiply_TwoLists([H1|L1],[H2|L2],Ans):- multiply_TwoLists(L1,L2,NewAnswer),    
                           Ans is H1 * H2 + NewAnswer. 

%_________________________________________________________________________________________________________________________

% transpose_Matrix2([[]|_],[]).
% transpose_Matrix2(Matrix,[Row|Rows]) :- transpose1stCol(Matrix,Row,RestMatrix), 
%                                        transpose_Matrix2(RestMatrix,Rows),!.
% transpose1stCol([],[],[]).
% transpose1stCol([[H|T]|Rows],[H|Hs],[T|Ts]):- transpose1stCol(Rows,Hs,Ts).

%_________________________________________________________________________________________________________

