%_______________________________________________________________________________

printListOfList(List):-
    flatten(List, L),
    write(L).
%________________________________________________________________________________
printListOfList2([]).
printListOfList2([H|T]):-
    (is_list(H) -> printListOfList2(H),printListOfList2(T) ;
                   write(H),write(" "),printListOfList2(T)).