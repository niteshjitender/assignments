
fun():-
    assert(db(a)),
    assert(db(b)),
    assert(db(c)),

    retract(db(X)),
    write(X),
    X == a.
