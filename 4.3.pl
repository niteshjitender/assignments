stringConcatenate(List,AnswerString):-
    % atom_string(',',Space),
    list_To_String(List,"",AnswerString).

list_To_String([],TempString,AnswerString):-AnswerString = TempString.
list_To_String([H|T],TempString,AnswerString):-
    atom_string(H,ResultStr),
    atom_string(' ',Space),
    string_concat(H, Space, ResultString),
    string_concat(TempString,ResultString,NewTempString),
    list_To_String(T,NewTempString,AnswerString).
    

stringConcatenate2(List,Ans):-
    atomics_to_string(List," ",Ans).