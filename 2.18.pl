:-include('out').

%________________________________________________________________________________________
%Main Function
polynomialDivision(Poly,Div,FinalQuotient,Remainder):-
    write('\n\n\n'),
    converter(Poly,Polynomial),
    converter(Div,Divisor),
    length(Polynomial, Int1),
    length(Divisor, Int2),
    QLen is Int1 - Int2  + 1,
    length(Quotient, QLen),
    maplist(between(0,0),Quotient),
    format('Polynomial Division : Divisor ~w and Dividend ~w\n',[Polynomial,Divisor]),
    divisor(Polynomial,Divisor,Quotient,Remainder,FinalQuotient),
    format('\n\n\nDivision Complete and the Final Answer is:-\n'),
    write('Quotient is '),output(FinalQuotient),
    write('\nRemainder is '),output(Remainder),!.
    % format('Quotient is ~w  and Remainder is ~w',[FinalQuotient,Remainder]).

%_________________________________________________________________________________________
%convertInput
converter([H|List],CList):-
    nth0(0,H,Coefficient),
    nth0(1,H,Power),
    ListLen is Power + 1,
    length(Temp,ListLen),
    maplist(between(0,0),Temp),
    worker([H|List],Temp,ListLen,CList).

 worker([],Temp,_,FinalList):- FinalList = Temp .
 worker([H|List],Temp,ListLen,FinalList):-
    nth0(0,H,Coefficient),
    nth0(1,H,Power),
    Index is ListLen - 1 - Power,
    replace(Index,Temp,Coefficient,NewTemp),
    worker(List,NewTemp,ListLen,FinalList).
    
%__________________________________________________________________________________________
%Division 
divisor(Polynomial,Divisor,Quotient,Remainder,FinalQuotient):-
    format('\nNext Division :\t\t\t~w / ~w',[Polynomial,Divisor]),
    length(Polynomial,PLen),
    length(Divisor,DLen),

    (PLen < DLen -> Remainder = Polynomial,FinalQuotient = Quotient;
                    
                    nth0(0,Polynomial,PHead),nth0(0,Divisor,DHead),  
                    Factor is PHead/DHead,
                    Temp is PLen - DLen + 1,
                    length(Quotient, Temp2),
                    Index is Temp2 - Temp,
                    replace(Index,Quotient,Factor,NewQuotient),
                    modifyPolynomial(Polynomial,Divisor,Factor,NewPolynomial),
                    divisor(NewPolynomial,Divisor,NewQuotient,Remainder,FinalQuotient)     
                                       
    ).
  %For Replacing the value at a index
  replace(Index,List,Value,FinalAnswer):-
      nth0(Index,List,_,Answer),
      nth0(Index,FinalAnswer,Value,Answer).

%_________________________________________________________________________________________________________________   
%_________________________________________________________________________________________________________________
%Reduce the Polynomial
modifyPolynomial(Polynomial,Divisor,Factor,NewPolynomial):-
    length(Polynomial, Plen),
    multiplyer(Divisor,Plen,Factor,Subtractor),
    maplist(minus, Polynomial,Subtractor,TempPolynomial),
    format('\n\nSubtraction of Resultant Pol. from the Dividend:\n\t\t\t\t\t\t   ~w\n\t\t\t\t\t\t  -~w\n\t\t\t\t\t\t  =~w',[Polynomial,Subtractor,TempPolynomial]),
    lead(TempPolynomial,NewPolynomial),
    format('\n\nContracted Polynomial: ~w',[NewPolynomial]).
  
  %Remove the inital Zeroes from the  Polynomial
  lead([],[]).
  lead([0.0|T],T2):-
    lead(T,T2).
  lead([0|T],T2) :-
     lead(T,T2).
  lead([H|T],[H|T]) :-
     dif(H,0).


  %Build the for subtraction                
  multiplyer(Divisor,Plen,Factor,Subtractor):-
      length(Divisor, Dlen),
      Extralen is Plen - Dlen,
      length(EmptyList, Extralen),
      maplist(between(0,0),EmptyList),
      fill(FactorList,Factor,Plen),
      append(Divisor,EmptyList,TempDivisor),
      maplist(mul,TempDivisor,FactorList,NewList),
      Subtractor = NewList.
  
  fill([], _, 0).
  fill([X|Xs], X, N) :- succ(N0, N), fill(Xs, X, N0).
%_______________________________________________________
%For Maplist:
minus(H1,H2,Ans):-
    Ans is H1 - H2.

mul(H1,H2,Value):-
    Value is H1 * H2.
 %_______________________________________________________   

    
    
    
    