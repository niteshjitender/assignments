% :- use_module(library(chr)).
% :- chr_constraint line/2, triangle/3.
% line(X,Y),line(X,Y) <=> line(X,Y).
% line(X,Y),line(Y,Z),line(X,Z) ==> triangle(X,Y,Z).

:- use_module(library(chr)).
:- chr_constraint line/2,
    triangle/3.
line(X,Y),line(Y,Z),line(X,Z) ==> triangle(X,Y,Z).
