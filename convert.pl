converter([H|List],CList):-
    nth0(0,H,Coefficient),
    nth0(1,H,Power),
    ListLen is Power + 1,
    length(Temp,ListLen),
    maplist(between(0,0),Temp),
    worker([H|List],Temp,ListLen,CList).

 worker([],Temp,_,FinalList):- FinalList = Temp .
 worker([H|List],Temp,ListLen,FinalList):-
    nth0(0,H,Coefficient),
    nth0(1,H,Power),
    Index is ListLen - 1 - Power,
    replace(Index,Temp,Coefficient,NewTemp),
    worker(List,NewTemp,ListLen,FinalList).
    
replace(Index,List,Value,FinalAnswer):-
    nth0(Index,List,_,Answer),
    nth0(Index,FinalAnswer,Value,Answer).
  