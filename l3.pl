
%__________________________________________________________
countElements1([],0).
countElements1([H|T],Count):-
    countElements1(T,NewCount),
    Count is NewCount + 1.
%___________________________________________________________

countElements2(List,Ans):-
    length(List,Ans).
%___________________________________________________________

countElements3(List,Ans):-
    flatten(List, FlatList),
    length(FlatList, Ans).
    
    