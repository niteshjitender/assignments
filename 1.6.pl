%Q6

maxOf4Numbers(W,X,Y,Z):-
    W>=X
       -> W>=Y
          -> W>=Z -> write(W) ; write(Z)
           ; Y>=Z -> write(Y) ; write(Z)
    ; X>=Y
       -> X>=Z
          -> write(X)
        ; write(Z)
    ; Y>=Z 
         -> write(Y)
         ; write(Z).


maxOf4Numbers_2(A, B, C, D)  :-
    A > B, A > C, A > D, write(A).
maxOf4Numbers_2(A, B, C, D)  :-
    B > A, B > C, B > D, write(B).
maxOf4Numbers_2(A, B, C, D)  :-
    C > A, C > B, C > D, write(C).
maxOf4Numbers_2(A, B, C, D)  :-
    D > A, D > B, D > C, write(D).

