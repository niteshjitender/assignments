%________________________________________________________________________________________
remove_duplicates(List):-
    duplicates(List,[]).

duplicates([],Answer):-
    write(Answer).
duplicates([H|List],Answer):-
    (member(H,Answer)->duplicates(List,Answer);
                       append(Answer,[H],NewList),duplicates(List,NewList)).

%___________________________________________________________________________________________

remove(List):-
    list_to_set(List,Set),
    write(Set).

