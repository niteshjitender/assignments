transpose_Matrix([H|Matrix],FinalAnswer):-
    length(H,LenOfRow),
    master([H|Matrix],0,LenOfRow,[],FinalAnswer),
    format('\n\n\t\t\tTranspose of the ~w is ~w\n',[[H|Matrix],FinalAnswer]).    

master(Matrix,Index,0,NewTempAnswer,FinalAnswer):-
    FinalAnswer = NewTempAnswer.
master(Matrix,Index,LenOfRow,TempAnswer,FinalAnswer):-
    worker(Matrix,Index,[],FinalList),
    NewIndex is Index + 1,
    New_LenOfRow is LenOfRow - 1,
    append(TempAnswer,[FinalList],NewTempAnswer),
    master(Matrix,NewIndex,New_LenOfRow,NewTempAnswer,FinalAnswer),!.

worker([],Index,NewTempList,FinalList):-
    FinalList = NewTempList.
worker([H|Matrix],Index,TempList,FinalList):-
    nth0(Index,H,Element),
    append(TempList,[Element],NewTempList),
    worker(Matrix,Index,NewTempList,FinalList),!.



multiplyMatrix2(Matrix1,Matrix2,Answer):-
    format('\nMatrix ~w and Matrix ~w',[[Matrix1],[Matrix2]]),
    transpose_Matrix(Matrix2,TransposedMatrix),
    format('\nMultiplication of Matrix1 * Matrix2 : ~w * ~w:-\n',[Matrix1,TransposedMatrix]),
    multiplyWorker(Matrix1,TransposedMatrix,Answer),
    format('\n\nFinal Answer of ~w * ~w is ~w',[Matrix1,TransposedMatrix,Answer]).

multiplyWorker([],_,[]).
multiplyWorker([Row|Rows],TransposedMatrix,[Result|Answer]):- 
    format('\n\t\t\t~w * ~w is ',[Row,TransposedMatrix]),
    multiply_ListWithMatrix(Row,TransposedMatrix,Result),
    format('~w',[Result]),
    multiplyWorker(Rows,TransposedMatrix,Answer).
                                                    
multiply_ListWithMatrix(_,[],[]).
multiply_ListWithMatrix(L1,[L2|T],[Sum|Row]):- maplist(prodNsum,L1,L2,NewList),
                                                  sum_list(NewList, Sum),
                                                  multiply_ListWithMatrix(L1,T,Row),!.
prodNsum(H1,H2,Ans):-
    Ans is H1 * H2.                          