%_______________________________________________________
findLastElement(List,Ans):-
    last(List,Ans).

%_______________________________________________________
findLastElement2([H|[]],Ans):-
    Ans is H.
findLastElement2([H|T],Ans):-
    findLastElement2(T,Ans).
    