add_at_middle_ListOfList(Element,List,Answer):-
    length(List,Len),
    Index is div(Len,2),
    nth0(Index, Answer,Element,List).