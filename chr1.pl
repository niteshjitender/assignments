:-use_module(library(chr)).
:-chr_constraint point/1, line/2, triangle/3.

% point(X), point(Y) ==>( retract(db1(Var1,Var2)) -> ((Y \= Var1 , X \= Var2),(Y \= Var2 , X \= Var1)); true)|line(X,Y),assert(db1(X,Y)).
% line(Y,X),line(Z,Y),line(Y,Z) ==> triangle(X,Y,Z).

point(X),point(Y) ==> line(X,Y).
% line(X,Y)\line(Y,X) <=> true.
line(X,Y),line(Y,X) <=> line(X,Y).

line(X,Y), line(Y,Z), line(X,Z) ==> triangle(X,Y,Z).
triangle(P,Q,R)\triangle(P,R,Q) <=> true.
triangle(P,Q,R)\triangle(Q,P,R) <=> true.
triangle(P,Q,R)\triangle(Q,R,P) <=> true. 
triangle(P,Q,R)\triangle(R,Q,P) <=> true.
triangle(P,Q,R)\triangle(R,P,Q) <=> true.  